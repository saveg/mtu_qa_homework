import { Browser, chromium, LaunchOptions } from "playwright";
import PageObject from "../src/component/PageObject";

let browser: Browser;
jest.setTimeout(310000);

describe.skip("Case", () => {
  afterEach(async () => {
    await browser.close();
  });

  it("Test", async () => {
    const launchOption: LaunchOptions = {
      devtools: true,
      headless: false,
    };
    browser = await chromium.launch(launchOption);
    const page = await PageObject.initPage(browser);
    await page.showPopup();
  });
});
