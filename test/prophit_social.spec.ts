import { Browser, chromium, LaunchOptions } from "playwright";
import ProfitSocialPo from "../src/page/ProfitSocialPo";

let browser: Browser;
jest.setTimeout(310000);

describe("Profit Social test", () => {
  afterEach(async () => {
    await browser.close();
  });

  it("Sign Up", async () => {
    const launchOption: LaunchOptions = {
      headless: false,
    };
    browser = await chromium.launch(launchOption);
    const page = await ProfitSocialPo.initPage(browser);
    await page.signUp();
    await page.page.waitForTimeout(20000);
  });
});
