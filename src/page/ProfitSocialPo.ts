import { Browser, Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";
import Select from "../component/Select";

export default class ProfitSocialPo {
  private signUpButton: WebElement;

  private countrySelect: Select;

  constructor(public page: Page) {
    this.signUpButton = new WebElement(this.page, "//div[@data-slideto = 'signup'][contains(@class, 'button')]");
    this.countrySelect = new Select(this.page, "//*[@id = 'country']/parent::*");
  }

  async signUp(): Promise<void> {
    await this.signUpButton.click();
    return this.countrySelect.select("Albania");
  }

  static async initPage(browser: Browser): Promise<ProfitSocialPo> {
    const page = await browser.newPage();
    await page.goto("https://www.profitsocial.com/en");
    return new ProfitSocialPo(page);
  }
}
