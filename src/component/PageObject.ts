import { Browser, Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";
import AllureHelper from "./AllureHelper";

export default class PageObject {
  private searchInput: WebElement;

  private searchPopup: WebElement;

  constructor(protected page: Page) {
    this.searchInput = new WebElement(this.page, "//div[@jscontroller = 'iDPoPb']");
    this.searchPopup = new WebElement(this.page, "//div[@jscontroller = 'tg8oTe']");
  }

  async showPopup(): Promise<void> {
    await this.searchInput?.userType("MTU school");
    await this.searchPopup?.waitForElement();
    return AllureHelper.makeElementScreenshot(this.searchPopup);
  }

  static async initPage(browser: Browser): Promise<PageObject> {
    const page = await browser.newPage();
    await page.goto("https://google.com");
    return new PageObject(page);
  }
}
