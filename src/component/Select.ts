import { Page } from "playwright";
import { WebElement } from "school-helper/lib/playwright";

export default class Select {
  private root: WebElement;

  private selectElement: WebElement;

  private popup: WebElement;

  constructor(protected page: Page, selector: string) {
    this.root = new WebElement(page, selector);
    this.selectElement = this.root.getChildElement("/div[contains(@class, 'select')]");
    this.popup = this.selectElement.getChildElement("//div[@class = 'select__dropdown']");
  }

  async select(option: string): Promise<void> {
    await this.selectElement.click();
    await this.popup.waitForElement(4000);
    const optionElement = this.getOptionByText(option);
    return optionElement.click();
  }

  getOptionByText(option: string): WebElement {
    return this.popup.getChildElement(`//div[@class='select__option'][text() = '${option}']`);
  }
}
