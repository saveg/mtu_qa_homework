import { WebElement } from "school-helper/lib/playwright";
import { NGlobal } from "../namespace/NGlobal";
import EContentType = NGlobal.EContentType;

export default class AllureHelper {
  static async makeElementScreenshot(element: WebElement, name = "Screenshot of element") {
    const screenshot = await element.screenshot();
    global.reporter.addAttachment(name, screenshot, EContentType.PNG);
  }
}
