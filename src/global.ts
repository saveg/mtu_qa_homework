import { Reporter } from "jest-allure/dist/Reporter";

declare global {
  namespace NodeJS {
    interface Global {
      reporter: Reporter,
      stopAllure: boolean,
    }
  }
}
